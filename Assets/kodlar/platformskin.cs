﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformskin : MonoBehaviour
{
    public Material[] materyaller;
    public int level, skin, skinkontrol;

    void Start()
    {
        level = PlayerPrefs.GetInt("level");
        skin = level + 3;
        skinkontrol = skin % 3;
    }

    void Update()
    {
        this.GetComponent<Renderer>().material = materyaller[skinkontrol];
    }
}
