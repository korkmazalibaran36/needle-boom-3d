﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bomba : MonoBehaviour
{
    public int patzaman;
    public bool patbool;
    public Animator myanim;
    public GameObject patlamago;
    public AudioSource bombaudio;
 

    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        myanim = this.GetComponent<Animator>();
        if (patbool)
        {
            patlamago.SetActive(true);
            patzaman--;
            if(patzaman <= 0)
            {
                patlamago.SetActive(false);
                patbool = false;

            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Balon")
        {
            bombaudio.Play();
            patzaman = 20;
            patbool = true;
            other.gameObject.SetActive(false);
        }
    }
}
