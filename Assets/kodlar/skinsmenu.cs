﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class skinsmenu : MonoBehaviour
{

    public int level, levelgerekli, buskin;
    public GameObject GameManager;

    void Start()
    {

    }

    void Update()
    {
      
        level = PlayerPrefs.GetInt("level");
        if(level < levelgerekli)
        {
            this.GetComponent<Button>().interactable = false;
        }
        else if (level >= levelgerekli)
        {
            this.GetComponent<Button>().interactable = true;
        }
        
    }
    public void tiklanma()
    {

        PlayerPrefs.SetInt("skin", buskin);
        GameManager.GetComponent<PlayUnityAd1>().showAd();
    }
}
