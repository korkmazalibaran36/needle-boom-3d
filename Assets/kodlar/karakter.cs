﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class karakter : MonoBehaviour
{
    public float hiz;
    public int level;
    public Text l1, l2;
    public bool sag, sol;
    public GameObject paneldie, dusmandegdi,butonsol,butonsag, panelwon,paneldie2, manager;
    public GameObject[] bolgeler;
    public AudioSource won,lost;

    void Start()
    {
        this.GetComponent<AudioSource>().Play();
        level = PlayerPrefs.GetInt("level");

        if (level == 0)
        {
            level = 1;
            PlayerPrefs.SetInt("level", level);
        }
        if (level < 40)
        {
            hiz = 1f + (level / 21f);

        }
        if (level >= 40 && level< 60)
        {
            hiz = 1f + (level / 25f);

        }
        if (level >= 60)
        {
            hiz = 1f + (level / 30f);

        }

    }

    void FixedUpdate()
    {            
        l1.text = level.ToString();
        l2.text = (level + 1).ToString();
        transform.Translate(-hiz, 0, 0);
        if (sag)
        {
            transform.Translate(0, -1f, 0);
        }
        if (sol)
        {
            transform.Translate(0, 1f, 0);

        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(0, 1f, 0);

        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(0, -1f, 0);

        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "bombatak")
        {
            other.gameObject.SetActive(false);
            manager.GetComponent<gamemanagerof>().score += 10;
        }
      if (other.gameObject.name == "dur")
        {
            lost.Play();
            paneldie.gameObject.SetActive(true);
            butonsag.gameObject.SetActive(false);
            butonsol.gameObject.SetActive(false);
            sol = false;
            sag = false;
            dusmandegdi = other.gameObject;
            Time.timeScale = 0;
            PlayerPrefs.SetInt("score", manager.GetComponent<gamemanagerof>().score);
        }
        if (other.gameObject.name == "dur1")
        {
            lost.Play();
            paneldie2.gameObject.SetActive(true);
            butonsag.gameObject.SetActive(false);
            butonsol.gameObject.SetActive(false);
            sol = false;
            sag = false;
            Time.timeScale = 0;
        }
        if (other.gameObject.name == "0")
        {
            bolgeler[0].gameObject.SetActive(true);
            manager.GetComponent<gamemanagerof>().score += 10;

        }
        if (other.gameObject.name == "1")
        {
            bolgeler[1].gameObject.SetActive(true);
            hiz = hiz * 1.05f;
            manager.GetComponent<gamemanagerof>().score += 100;

        }
        if (other.gameObject.name == "2")
        {
            bolgeler[2].gameObject.SetActive(true);
            manager.GetComponent<gamemanagerof>().score += 150;

        }
        if (other.gameObject.name == "3")
        {
            bolgeler[3].gameObject.SetActive(true);
            hiz = hiz * 1.05f;
            manager.GetComponent<gamemanagerof>().score += 170;

        }
        if (other.gameObject.name == "4")
        {
            bolgeler[4].gameObject.SetActive(true);
            manager.GetComponent<gamemanagerof>().score += 180;

        }
        if (other.gameObject.name == "5")
        {
            bolgeler[5].gameObject.SetActive(true);
            manager.GetComponent<gamemanagerof>().score += 180;

        }
        if (other.gameObject.name == "6")
        {
            bolgeler[6].gameObject.SetActive(true);
            hiz = hiz * 1.05f;
            manager.GetComponent<gamemanagerof>().score += 200;

        }
        if (other.gameObject.name == "7")
        {
            bolgeler[7].gameObject.SetActive(true);
            manager.GetComponent<gamemanagerof>().score += 200;

        }
        if (other.gameObject.name == "8")
        {
            bolgeler[8].gameObject.SetActive(true);
            hiz = hiz * 1.05f;
            manager.GetComponent<gamemanagerof>().score += 210;

        }
        if (other.gameObject.name == "9")
        {
            bolgeler[9].gameObject.SetActive(true);
            hiz = hiz * 1.05f;
            manager.GetComponent<gamemanagerof>().score += 500;

        }
        if(other.gameObject.name == "yesil")
        {
            manager.GetComponent<gamemanagerof>().score += 50;

        }
        if (other.gameObject.name == "mavi")
        {
            manager.GetComponent<gamemanagerof>().score += 100;

        }
        if (other.gameObject.name == "sari")
        {
            manager.GetComponent<gamemanagerof>().score += 150;

        }
        if (other.gameObject.name == "turuncu")
        {
            manager.GetComponent<gamemanagerof>().score += 200;

        }
        if (other.gameObject.name == "mor")
        {
            manager.GetComponent<gamemanagerof>().score += 300;

        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "finish")
        {
            won.Play();
            level += 1;
            PlayerPrefs.SetInt("score", manager.GetComponent<gamemanagerof>().score);
            PlayerPrefs.SetInt("level", level);
            panelwon.gameObject.SetActive(true);
            bolgeler[10].gameObject.SetActive(true);
            Time.timeScale = 0;
        }
    }


    #region kontroller
    public void saga()
    {
        sag = true;
    }
    public void sagbirak()
    {
        sag = false;
    }
   public void sola()
    {
        sol = true;
    }
    public void solbirak()
    {
        sol = false;
    }
    #endregion
}
