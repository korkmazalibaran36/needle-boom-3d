﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skins : MonoBehaviour
{
    public int skinno;
    public Material[] mymats;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        skinno = PlayerPrefs.GetInt("skin");
        this.GetComponent<Renderer>().material.color = mymats[skinno].color;
    }
}
