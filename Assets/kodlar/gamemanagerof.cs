﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gamemanagerof : MonoBehaviour
{
    public GameObject karakter, start, scorego, removepanel, removebutton;
    public int score, best;
    public Text scoretxt, besttxt;
    public int level;

    void Start()
    {
        score = PlayerPrefs.GetInt("score");
        Time.timeScale = 1;
        karakter.GetComponent<karakter>().enabled = false;
        start.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerPrefs.GetInt("removeads") == 1)
        {
            this.GetComponent<PlayUnityAd1>().enabled = false;
            removepanel.SetActive(false);
            removebutton.SetActive(false);
        }
        besttxt.text = best.ToString();
        scoretxt.text = score.ToString();
        best = PlayerPrefs.GetInt("best");
        level = PlayerPrefs.GetInt("level");
        if(score> best)
        {
            PlayerPrefs.SetInt("best", score);
        }
    }
    public void playbasildi()
    {
        scorego.SetActive(true);
        karakter.GetComponent<karakter>().enabled = true;
        start.gameObject.SetActive(false);
    }
    public void yeniden()
    {
        Application.LoadLevel(0);
    }
    public  void sifirla()
    {
        PlayerPrefs.SetInt("score", 0);

    }
    public  void remove_ackapa(bool aqq)
    {
        removepanel.SetActive(aqq);

    }
    public  void removeads()
    {
        PlayerPrefs.SetInt("removeads", 1);
    }
}
