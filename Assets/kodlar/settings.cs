﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class settings : MonoBehaviour
{
    public GameObject sesac1, seskapat1, sesac2, seskapat2, sesac3, seskapat3;
    public AudioSource music, bomb, otherses1, otherses2;
    public int a, b, c;

    void Start()
    {
        
    }

    void Update()
    {
        a = PlayerPrefs.GetInt("music");
        b = PlayerPrefs.GetInt("peopleses");
        c = PlayerPrefs.GetInt("bombses");
        if(a == 0)
        {
            sesac1.SetActive(true);
            seskapat1.SetActive(false);
            music.volume = 1;
            bomb.volume = 1;
            otherses1.volume = 1;
            otherses2.volume = 1;
        }
        if (a == 1)
        {
            sesac1.SetActive(false);
            seskapat1.SetActive(true);
            music.volume = 0;
            bomb.volume = 0;
            otherses1.volume = 0;
            otherses2.volume = 0;
        }
        if (b == 0)
        {
            sesac2.SetActive(true);
            seskapat2.SetActive(false);
            otherses1.volume = 1;
            otherses2.volume = 1;
        }
        if (b == 1)
        {
            sesac2.SetActive(false);
            seskapat2.SetActive(true);
            otherses1.volume = 0;
            otherses2.volume = 0;
        }
        if (c == 0)
        {
            sesac3.SetActive(true);
            seskapat3.SetActive(false);
            bomb.volume = 1;
        }
        if (c == 1)
        {
            sesac3.SetActive(false);
            seskapat3.SetActive(true);
            bomb.volume = 0;
        }
    }
    public void sesayarlapeople()
    {
        if (b == 0)
        {
            PlayerPrefs.SetInt("peopleses", 1);
        }
        if (b == 1)
        {
            PlayerPrefs.SetInt("peopleses", 0);
        }
    }
    public void musicayarla()
    {
        if (a == 0)
        {
            PlayerPrefs.SetInt("music", 1);
        }
        if (a == 1)
        {
            PlayerPrefs.SetInt("music", 0);
        }
    }
    public void bombayarla()
    {
        if (c == 0)
        {
            PlayerPrefs.SetInt("bombses", 1);
        }
        if (c == 1)
        {
            PlayerPrefs.SetInt("bombses", 0);
        }
    }
    public void openplay()
    {
        Application.OpenURL("https://play.google.com/store/apps/dev?id=6545803624913232890");
    }
}
