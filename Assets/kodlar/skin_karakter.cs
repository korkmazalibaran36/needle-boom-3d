﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skin_karakter : MonoBehaviour
{
    public int skin;
    public GameObject needle, pencil, ok2, cross2, bic, bickin;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        skin = PlayerPrefs.GetInt("skin");
        if(skin == 0)
        {
            needle.gameObject.SetActive(true);
            pencil.gameObject.SetActive(false);
            ok2.gameObject.SetActive(false);
            cross2.gameObject.SetActive(false);
            bic.gameObject.SetActive(false);
            bickin.gameObject.SetActive(false);
            
        }
        else if(skin == 1)
        {
            needle.gameObject.SetActive(false);
            pencil.gameObject.SetActive(true);
            ok2.gameObject.SetActive(false);
            cross2.gameObject.SetActive(false);
            bic.gameObject.SetActive(false);
            bickin.gameObject.SetActive(false);
            
        }
        else if(skin == 2)
        {
            needle.gameObject.SetActive(false);
            pencil.gameObject.SetActive(false);
            ok2.gameObject.SetActive(true);
            cross2.gameObject.SetActive(true);
            bic.gameObject.SetActive(false);
            bickin.gameObject.SetActive(false);
            
        }
        else if(skin == 3)
        {
            needle.gameObject.SetActive(false);
            pencil.gameObject.SetActive(false);
            ok2.gameObject.SetActive(false);
            cross2.gameObject.SetActive(false);
            bic.gameObject.SetActive(true);
            bickin.gameObject.SetActive(true);
            
        }
    }
}
