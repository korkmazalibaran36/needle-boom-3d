﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class change_sky : MonoBehaviour
{
    public Material[] skies;
    public int level;

    void Start()
    {
        level = PlayerPrefs.GetInt("level");
        if(level % 2 == 0)
        {
            RenderSettings.skybox = skies[0];
        }
        if (level % 2 == 1)
        {
            RenderSettings.skybox = skies[1];
        }
    }

    void Update()
    {
        
    }
}
